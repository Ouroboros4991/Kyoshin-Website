<?php
/**
 * Created by PhpStorm.
 * User: ruben
 * Date: 21/09/2016
 * Time: 12:26
 */
require_once("private/utils.php");
start_session();

require_once "private/top.php";
require_once "private/user.php";
require_once "private/posts.php";

if(isset($_SESSION['username']) && $_SESSION['username']==='admin' ){
    showPostForm();
}
showAllPosts();


require_once 'private/bottom.php';
