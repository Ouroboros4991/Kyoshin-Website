<?php
/**
 * Created by PhpStorm.
 * User: ruben
 * Date: 20/09/2016
 * Time: 22:01
 */

 require_once("private/utils.php");
 start_session();

 require_once "private/top.php";

 ?>
<article>
    <h2>Kyoshin Dojo</h2>
    <p>
        Bij ons leert u de kunst van zelfverdediging. U leert jezelf te verdedigen in elke denkbare situatie.
        Situaties gebaseerd op het hedendaagse straatgeweld. Discipline en respect staan bij ons centraal.
        Kortom, een heel toffe sport waarbij je uw conditie en zelfvertrouwen sterk kan verbeteren.
        Onze jeugdwerking zorgt ervoor dat de Pre-Miniemtjes, Miniemtjes, Kadetjes en Juniors goed worden begeleid.
        Naast techniek worden er ook sociale vaardigheden en respect aangeleerd.
    </p>
</article>
<article>
    <h2>Voor wie?</h2>
    <p>Onze Jiu-Jitsu sport is voor jong en oud:</p>
    <ul>
        <li>Vanaf 4 jaar tot 99 jaar</li>
        <li><em>Miniemtjes (Weg van de kleinde draak):</em> 4 tot 7 jaar. </li>
        <li><em>Kadetjes: </em> 8 tot 9 jaar</li>
        <li><em>Juniors: </em> 10 tot 14 jaar</li>
        <li><em>Seniors: </em> 14 tot 39 jaar</li>
        <li><em>Masters: </em> 40+ jaar</li>
    </ul>

    <p>Iedereen die zich soms wel eens onveilig voelt of die zich gewoon wilt leren verdedigen</p>

    <p>
        <strong> Bij ons maak je vrienden en loop je niet verloren!!!</strong>
    </p>
</article>
<?php
require_once "private/bottom.php";
