<?php
/**
 * Created by PhpStorm.
 * User: ruben
 * Date: 21/09/2016
 * Time: 12:26
 */
require_once("private/utils.php");
start_session();

require_once "private/top.php";
require_once "private/user.php";


if(isset($_SESSION['username'])){
    echo "<h1>Pagina voor lid </h1>";
}
else{
    header("Location: login.php");
}


require_once 'private/bottom.php';
