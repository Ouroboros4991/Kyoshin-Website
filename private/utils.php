<?php

function start_session(){
    $session_name = 'sec_session_id';
    session_name($session_name);

    ini_set('session.cookie_httponly', 1);
    ini_set('session.use_only_cookies', 1);
    ini_set('session.use_strict_mode',1);
    ini_set('session.hash_function','sha512');
    ini_set('session.hash_bits_per_character',6);


    session_start();
    

}

function validateInput($input){
    $result = htmlentities($input);
    return $result;
}

function checkToken(){
    if(isset($_POST['_csrfname']) && isset($_POST['_csrfvalue'] )&& isset($_SESSION[$_POST['_csrfname']]) && $_SESSION[$_POST['_csrfname']] === $_POST['_csrfvalue']){
            return true;
    }else{
        return false;
    }
}

//TODO error message function
