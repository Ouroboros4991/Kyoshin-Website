<?php
require_once "private/db.php";

function getPassword($username){
    $database = new database();
    $db = $database->getdb();

    $sql = 'SELECT wachtwoord FROM users where username = :user';
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':user',$username);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_CLASS);

    $stmt = null;
    $database->sluitdb();
    if(isset($result[0])){
            return $result[0]->wachtwoord;
    }else{
        return null;
    }
}

/*function getAdminRights($username){
    $database = new database();
    $db = $database->getdb();

    $sql = 'SELECT adminRights FROM users where username = :user';
    $stmt =$db->prepare($sql);
    $stmt->bindParam(':user',$username);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_CLASS);

    $stmt = null;
    $database->sluitdb();
    if(isset($result[0])){
            return $result[0]->adminRights;
    }else{
        return null;
    }
}*/

 ?>
