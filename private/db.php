<?php
/**
 *
 */
 //TODO 2 nieuwe klasses maken voor attemps en username
 require_once("private/config.php");
class database
{
    private static $databaseInstantie = null;
    private $db;
    function __construct()
    {
        try{
        $server = config::getConfigInstantie()->getServer();
        $database = config::getConfigInstantie()->getDatabase();
        $username = config::getConfigInstantie()->getUsername();
        $password = config::getConfigInstantie()->getPassword();
        $this->db = new PDO("mysql:host=$server; dbname=$database",$username,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e ){
        die($e->getMessage());
        }
    }

    public static function getInstantie(){
        if(is_null(self::databaseInstantie)){
            self::$databaseInstantie = new database();
        }
        return self::$databaseInstantie;
    }

    public function sluitdb(){
        $this->db = null;
    }

    public function getdb(){
        return $this->db;
    }


}


 ?>
