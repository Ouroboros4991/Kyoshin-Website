<?php
    header('Content-Security-Policy: default-src \'self\'');
    header('X-FRAME-OPTIONS','DENY');
    session_regenerate_id(true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="Assets/CSS/reset.css"/>
    <link rel="stylesheet" type="text/css" href="Assets/CSS/screen.css"/>
    <meta name="author" content="Ruben Dejaegere"/>
    <title>Kyoshin jiu-jutsu club</title>
</head>
<body>
<div id="container">
    <header>

            <a href="index.php" id="top"><img src="Images/logo_tmp.PNG" alt="logo" title="logo"> </a>

        <nav>
            <div class="dropdown">
                <h2> <a href="nieuws.php"> Nieuws</a> </h2>
                <ul>
                    <li> <a href="kalender.php">Kalender</a></li>

                </ul>
            </div>

            <div class="dropdown">
                <h2> <a href="jiu-jutsu.php"> Jiu-Jitsu </a></h2>
                <ul>
                    <li> <a href="geschiedenis.php"> Geschiedenis van Jiu-Jitsu </a></li>
                    <li> <a href="woorden.php">Japanse woorden </a></li>
                </ul>
            </div>

            <div class="dropdown">
                <h2><a href="dojo.php"> Dojo </a></h2>
                <ul>
                    <li> <a href="#"> Sensei's </a></li>
                    <li> <a href="#"> Federatie's </a></li>
                    <li> <a href="#"> Links & Friends </a></li>
                </ul>
            </div>

            <div class="dropdown">
                <h2> <a href="lid.php">Leden </a></h2>
                <ul>
                    <li> <a href="kalender.php">Foto's</a></li>
                    <li> <a href="kalender.php">Video's</a></li>
                </ul>
            </div>
            <?php
                if(isset($_SESSION['username'])){
                    if($_SESSION['username']==='admin'){
                        echo '<a href="admin.php" id="admin">Admin</a>';
                    };

                    echo '<div class="dropdown"><h2> <a href="logout.php">Logout </a></h2></div>';



                }
            ?>

        </nav>
    </header>
    <section id="center">
