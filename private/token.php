<?php


class token{
    private $tokenname;
    private $token;

    function __construct(){
        $this->tokenname = 'token-'.mt_rand();
        $this->token = bin2hex(random_bytes(32));
        $_SESSION[$this->tokenname] = $this->token;
    }

    function createTokenHTML(){
        ?>
        <input type="hidden" name="_csrfname" value="<?php echo $this->tokenname; ?>">
        <input type="hidden" name="_csrfvalue" value="<?php echo $this->token; ?>">
        <?php
    }

}


 ?>
