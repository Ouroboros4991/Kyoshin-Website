<?php
require_once("private/token.php");
require_once("private/userDB.php");

function showLoginHTML(){
        ?>
        <form action="" method="post" id="loginForm">
        <?php
        $token = new token();
        $token->createTokenHTML();
        ?>

        <p><label for="username">Username: </label>
        <input type="text" id="username" name="username" /> </p>
        <p><label for="password">Password: </label>
            <input type="password" id="password" name="password"></p>
        <input type="submit" value="Login"/>
        </form>
    <?php

}

function handleLogin($username,$password){
    if(checkPassword($username,$password)){
            $_SESSION['username'] = $username;
            return true;
        }
    return false;


}

function checkPassword($username,$password){
    $hash = getPassword($username);
    if($hash != null){
        return password_verify($password,$hash);
    }else{
        return false;
    }
}


 ?>
