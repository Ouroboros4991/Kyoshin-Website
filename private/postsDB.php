<?php
require_once("private/db.php");

function addNewPost($title, $message) {
    $database = new database();
    $db = $database->getdb();
    $second= date("s");
    $minute= date("i");
    $hour= date("H") - date("I") + 2;
    $day= date("d");
    $month= date("m");
    $year= date("Y");

    try {
        $sql = "INSERT INTO posts(pSecond, pMinute, pHour, pDay, pMonth, pYear, pUser, pTitle, pContent) VALUES($second, $minute, $hour, $day, $month, $year, 'Kyoshin', :title, :message)";
        $stmt =  $db->prepare($sql);
        //$stmt->bindParam(":pUser", 'Default user');
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":message", $message);
        $stmt->execute();
        $stmt = null;
        $database->sluitdb();
    }
    catch(PDOException $e) {

        die($e->getMessage());

    }
}

function getPosts() {
    $database = new database();
    $db = $database->getdb();

    try {

        $sql = "SELECT * FROM posts ORDER BY pYear DESC, pMonth DESC, pDay DESC, pHour DESC, pMinute DESC, pSecond DESC";
        $stmt =  $db->prepare($sql);
        $stmt->execute();
        $resultaat = $stmt->fetchAll(PDO::FETCH_CLASS);

        $stmt = null;
        $database->sluitdb();
        return $resultaat;

    }

    catch(PDOException $e) {

        die($e->getMessage());

    }

}

?>
