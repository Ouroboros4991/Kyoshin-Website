<?php
require_once("private/token.php");
require_once("private/postsDB.php");

function showPostForm()
{

    ?>
    <button type="button" id='addPostBtn'>Add new Post</button>

    <form action="addPost.php" method="post" id = "postForm" class="hidden">
        <?php
        $token = new token();
        $token->createTokenHTML();
        ?>
        <h3>New post</h3>
        <fieldset>
            <p>
                <label for="title">Title</label>
                <input type="text" name="title" id="title" placeholder="title">
            </p>
            </p>
            <label for="content">Message</label>
            <textarea name="content" id="content" class="validateLength" placeholder="message"></textarea>
            </p>
            <aside class="remaining"><!-- X chars remaining --></aside>

            <ul>
                <li>
                    <input type="submit" name="post" id="post" value="post!">
                </li>
                <li>
                    <input type="reset" value="clear">
                </li>
            </ul>
        </fieldset>
    </form>

    <?php

}

function showAllPosts()
{
    $data = getPosts();
    $resultaat = "<section id='wall'><h2>Nieuws</h3>";

    foreach ($data as $record) {

        $resultaat .= "<article><h3>" . $record->pTitle. "</h3>";
        $resultaat .= "<aside>" . $record->pHour. "h" . $record->pMinute . "</aside>";
        $resultaat .= "<p>" . $record->pContent. "</p></article>";

    }

    echo $resultaat;

}

?>
