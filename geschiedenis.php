<?php
/**
 * Created by PhpStorm.
 * User: ruben
 * Date: 20/09/2016
 * Time: 21:43
 */
 require_once("private/utils.php");
 start_session();

 require_once "private/top.php";
?>
<h1>Geschiedenis</h1>
<article>
    <h2>Jiu-Jitsu , Moeder van alle Japanse gevecht - en verdedigingssporten!</h2>

    <p>
        Alhoewel de roots van de Japanse Martial Arts in de beginne waren overgewaaid vanuit China, Korea en India,
        is het huidige Jiu-Jitsu toch hoofdzakelijk ontwikkeld door Samurai en het Japanse volk. Zij vertrokken hierbij
        vanuit de overgewaaide basis principes van de Martiale kunsten.
    </p>
    <p>
        Ondertussen zijn sporten zoals Judo, Karate en Aikido beter gekend bij het grote publiek dan Jiu-Jitsu. Maar wat
        veel mensen niet weten is dat de eerder genoemde sporten met uitzondering van het Okinawa Karate, allemaal sporten
        zijn die ontstaan zijn vanuit het Jiu-Jitsu. Hun stichters waren bedreven Jiu-Jitsuka maar hadden zich
        gespecialiseerd in bepaalde technieken. Meester Jigoro Kano bijvoorbeeld, stichter van het Judo, nam alle minst
        gevaarlijke bewegingen uit het Jiu-Jitsu en ontwikkelde er mee een spelvorm waar werptechnieken centraal staan.
        En zo werd Judo geboren.
    </p>
    <p>
        O'Sensei, de oprichter van de Aikido, Morihei Ueshiba, houd sterk aan de filisofie van de Budo sporten.
        Hij draagt de oude waarden hoog in het vaandel en richt zich tot de oudere technieken en voert deze uit in
        harmonie met lichaam en geest.
    </p>
    <p>
        Meester Gichin Funakoshi, bezieler van het Shotokan Karate is een uitzondering. Hij specialiseerde zich dan weer in
        de vele stoot en traptechnieken maar haalde die hoofdzakelijk bij de roots van het Okinawa Karate. Maar daar
        Okinawa Karate in het oude Japan zowat een simultaan bestaan had naast het Jiu-Jitsu (in het oude Japan “Yaware”
        genaamd), kunnen wij stellen dat de twee stijlen elkaar op zijn minst beïnvloeden.
    </p>
    <p>
        In sommige scholen (Ryu) smolten de technieken samen. Een mooi voorbeeld hiervan is Kobudo, het gewapend vechten en verdedigen.
    </p>
    <p>
        Al deze sporten zijn relatief jong. Ze zijn ontstaan in de 19e en 20e eeuw. Tot de dag van vandaag zijn deze sporten
        verder in ontwikkeling waarbij nog vaak nieuwe soorten en stijlen ontstaan. Ze vinden bijna allemaal hun roots bij het Jiu-Jitsu.
    </p>
    <p>
        Met deze stellingen kan men er van uitgaan dat Jiu-Jitsu de Moeder is van alle Japanse gevecht-en verdedigingssporten.
        Uiteraard heeft onze Jiu-Jitsu zich ook verder ontwikkeld en aangepast aan het hedendaagse straatgeweld.
    </p>
</article>
<article>
    <h3>Maar wat is nu de geschiedenis van het Jiu-Jitsu?</h3>
    <p>
        Wij weten dat sinds het ontstaan van de mens waar ook ter wereld conflict, combat, gevecht…het zij aanvallend
        het zij verdedigend altijd bestaan heeft. Het zit als het ware in de nature van de mens. In de oude wereld was
        dit noodzakelijk om te overleven.
    </p>
    <p>
        Ook in het oude Japan waren er prille vormen van combat. Maar hoe deze prille vorm uitgroeide tot de
        fascinerende gevechts kunsten zoals wij deze kennen vandaag is en blijft een vraagteken.
        Geschiedkundig hebben wij natuurlijk wel een schat aan informatie betreft de éérste Jiu-Jitsu scholen tot
        de dag van vandaag. Maar dat prille begin tot het onstaan van de éérste scholen in het oude Japan blijft een beetje vaag.
    </p>
    <p>
        Er zijn zo veel verhalen terug te vinden op het internet, in boeken…zo vele interpretaties.
        Je ziet de bomen door het bos niet. Wat is nu het juiste verhaal?
    </p>
    <p>
        Waarschijnlijk zullen deze verhalen wel allemaal een kern van waarheid verbergen maar om alsnog een beeld te
        kunnen geven neem ik graag het meest gekende verhaal om wat licht in het duister te scheppen.
    </p>
</article>
<article>
    <h3>Het verhaal van Akijama.</h3>
    <p>
        Rond 1690 werkte en studeerde Akijama een arts uit Japan in China.
    </p>
    <p>
        Tijdens een bezoek aan een klooster zag hij een gevecht tussen een gewapende en een ongewapende man.
        Tot zijn verbazing wist de ongewapende man met een speciale gevechtsmethode het gevecht te winnen.
        Akijama trad toe tot de kloosterorde en liet zich inwijden in de gevechtstechniek.
    </p>
    <p>
        Toen Akijama na vele jaren studie terug was in Japan en hij over deze technieken nadacht bleef hem een probleem dwarszitten.
        Hij wist nu wel de uitwerking van de grepen maar zelf had hij nog geen methode om zich te weren als de grepen op hem werden toegepast.
    </p>
    <p>
        In de winter zag hij hoe de takken van een kerseboom en een wilgeboom heel verschillend reageerden op de sneeuw die zij moesten dragen.
        De takken van de kerseboom braken af, terwijl de wilgeboom zijn takken liet afhangen waardoor de sneeuw er van afgleed.
        Akijama had nu de oplossing van zijn probleem: "Meegeven om te Overwinnen".
        Met deze gedachte als uitgangspunt lukte het hem om zich uit de moeilijkste grepen te bevrijden. De eerst stappen in
        het Jiu-Jitsu waren gezet.
    </p>
    <p>
        Vooral de samoerai gingen zich in Jiu-Jitsu bekwamen, als zij in een gevecht ontwapend werden dan konden ze zonder wapens verder strijden.
        In de eerste scholen (ryu) werd Jiu-Jitsu gegeven als gewapende en ongewapende krijgskunst.
        Zoals eerder vermeld waren de technieken ook bekend onder andere namen zoals Yawara en Kempo..
        Op dat moment was Jiu-Jitsu een geheime vechtkunst alleen voorbehouden aan de samoerai.
        De technieken zijn in Japan tot ontwikkeling gekomen door langdurige verfijning van de krijgskunsten.
    </p>
    <p>
        Ongeveer 200 jaar hebben de samoerai Jiu-Jitsu geheimgehouden tot in 1868 er een einde kwam aan het feodale stelsel in Japan.
        Veel samoerai waren zonder werk en gingen toen om in hun levensonderhoud te kunnen voorzien lesgeven in Jiu-Jitsu.
        In die tijd zijn er verschillende Jiu-Jitsu stijlen en scholen ontstaan, veel mensen raakten in de ban van deze doeltreffende zelfverdediging technieken.
        Al snel werd Jiu-Jitsu een populaire kunst in Japan.
    </p>
</article>
<article>
    <h3>Jiu-Jitsu in België.</h3>
    <p>
        Na het overwaaien van deze mooie kunst naar Europa en de rest van de wereld was er uiteraard een tijd dat
        Jiu-Jitsu zijn intrede deed in België.
    </p>
    <p>
        Anno 1902 was er een heer Alexander Minne die Jiu-Jitsu leerde via Japanse kampioenen zoals Taro Myake en
        Yukio Tani. Al snel begon Alexander Minne, bijgenaamd “Ito” ook les te geven in Jiu-Jitsu. Een tijdje later
        nam zijn broer Maurice Minne, bijgenaamd “Okita” de fakkel over.
    </p>
    <p>
        In 1954 stichte Meester Minne de éérste Jiu-Jitsu federatie in België.
        Zijn leerlingen waren op hun beurt verantwoordelijk voor het verder verspreiden van Jiu-Jitsu tot in de verste hoekjes van ons land.
    </p>
</article>
<?php
require_once "private/bottom.php";
