<?php
/**
 * Created by PhpStorm.
 * User: ruben
 * Date: 20/09/2016
 * Time: 21:21
 */
 require_once("private/utils.php");
 start_session();

 require_once "private/top.php";
?>
<article>
    <h2>Wat is Jiu-Jitsu?</h2>
    <p>
        Jiu-Jitsu legt de nadruk op de verdediging en niet op de aanval. "Jiu" betekent zacht of soepel, en "Jitsu"
        betekent kunst of vaardigheid. De letterlijke vertaling van "Jiu-Jitsu" is dus zachte kunst of soepele
        vaardigheid. Dit betekent niet dat de sport pijnloos of ongevaarlijk is. De kunst en de vaardigheid van onze
        sport zorgt ook wel eens voor spectaculaire bewegingen. Bij gevorderden kan het er vaak hard - maar altijd
        gecontroleerd - aan toe gaan.
    </p>
    <p>
        Het is een zelfverdedigingskunst waarmee je in een paar seconden een aanvaller kunt controleren en/of
        uitschakelen. Jiu-Jitsu is niet alleen een zelfverdedigingskunst, maar een zeer complete budo kunst voor jong
        en oud. In het Jiu-Jitsu leer je niet alleen je te verdedigen tegen verschillende aanvallen maar ook het
        uitvoeren van verschillende aanvalstechnieken zoals bijvoorbeeld atemi`s (stoten en trappen), klemmen,
        drukpunten en wurgingen maar steeds in een verdedigend perspectief. Het is de zelfverdedigingssport bij uitstek.
    </p>
</article>
<article>
    <h2>Jiu-Jitsu is méér dan sport alleen...</h2>
    <p>
        Naast het sportieve heeft Jiu-Jitsu voor ons ook nog vele andere betekenissen. Vriendschap, kameraadschap,
        discipline, beleefdheid zijn een directe greep uit de waarden van onze club. Jiu-Jitsu is niet enkel
        de fysiek maar is ook elkaar helpen op en naast de Tatami (de mat). Jiu-Jitsu is niet enkel presteren in
        onze sport maar is ook je best doen op school, op het werk en thuis. Jiu-Jitsu is open staan voor iedereen,
        ongeacht origine, uiterlijk en kunnen van de individu. Het is onze plicht Jiu-Jitsu op deze manier te ademen;
        met verwijzing naar onze clubnaam, Kyoshin
    </p>
</article>
<article>
    <h2>De 10 Waarden Van Kyoshin Jiu-Jitsu Club Diksmuide...</h2>
    <dl>
        <dt>01: Zelfbeheersing</dt>
        <dd> Een vermeden gevecht is een gewonnen gevecht.</dd>
        <dt>02: Respect</dt>
        <dd> Behandel anderen zoals jij zou willen behandeld worden.</dd>
        <dt>03: Discipline</dt>
        <dd> Toon inzet en probeer steeds je best te doen.</dd>
        <dt>04: Beleefdheid</dt>
        <dd> Wees steeds vriendelijk en beleefd.</dd>
        <dt>05: Karakter</dt>
        <dd> Probeer steeds een goed persoon te zijn.</dd>
        <dt>06: Eerlijkheid</dt>
        <dd> Vertel steeds de waarheid, eerlijkheid duurt het langst.</dd>
        <dt>07: Vriendschap</dt>
        <dd> Wees sociaal, maak vrienden en behoud een open geest.</dd>
        <dt>08: Compassie</dt>
        <dd> Leef mee en zie de wereld door anderen hun ogen.</dd>
        <dt>09: Verdraagzaamheid</dt>
        <dd> Leer relativeren en bezin.</dd>
        <dt>10: Wijsheid</dt>
        <dd> Ken jezelf en onderschat nooit je tegenstander.</dd>

    </dl>
</article>

<?php
require_once "private/bottom.php";
