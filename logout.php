<?php
require_once('private/utils.php');
start_session();

header('Content-Security-Policy: default-src \'self\'');
header('X-FRAME-OPTIONS','DENY');
session_regenerate_id(true);

    if(isset($_SESSION['username'])){
        setcookie(session_name(),'',30);
        session_unset();
        session_destroy();
        $_SESSION = array();
        header("Location: login.php");
    }else{
        echo "error";
    } ?>
