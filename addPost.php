<?php
require_once("private/utils.php");
start_session();

require_once "private/top.php";
require_once "private/user.php";
require_once "private/postsDB.php";

if(isset($_SESSION['username']) && $_SESSION['username']==='admin' ){
    $title = htmlentities($_POST['title']);
    $message = htmlentities($_POST['content']);


    addNewPost($title,$message);
    header("Location:nieuws.php");

}else{
    echo "error";
}


require_once "private/bottom.php";
?>
