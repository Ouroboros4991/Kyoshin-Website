<?php
/**
 * Created by PhpStorm.
 * User: ruben
 * Date: 21/09/2016
 * Time: 12:26
 */
require_once("private/utils.php");
start_session();

require_once "private/top.php";
require_once "private/user.php";


if(!isset($_SESSION['username'])){
    if(isset($_POST["username"])){
        if(checkToken()){
            $username = validateInput($_POST["username"]);
            $password = validateInput($_POST["password"]);
            if(handleLogin($username,$password)){
                header("Location: index.php");
            }else{
                echo "error";
                    }
            }
    	}
    else{
            showLoginHTML();
    }

}
else{
        echo "error";
}


require_once 'private/bottom.php';
